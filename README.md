# IEEE Pandoc Template

This repo aims to provide you with the tools to effortlessly compile IEEE papers using pandoc.

It follows requirements from the [IEEE's conference template](https://www.ieee.org/conferences/publishing/templates.html) (status oct. 2019).

## Pre-requisites

- pandoc v2.11
- tex packages
- a paper
- the template from this repo

## Installation

- copy template into Pandoc's template directory

## Usage

- define bibliography and citation style in the YAML header
  - you can e.g. copy both files into the directory of your paper and just quote the file names in the YAML header – Pandoc will interpret these as relative paths
- run the following command from inside the directory of your paper:\  `pandoc YOUR-PAPER.md -o YOUR-PAPER.pdf --citeproc --template="ieee"`
- have fun submitting your paper :)

## Attribution

- this is a fork from [Santos Gallegos' work](https://github.com/stsewd/ieee-pandoc-template)
